import { createStore, combineReducers } from "redux";
import { persistStore, persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";
import accessStore from "./access";

const reducers = combineReducers({
  accessStore,
});
const persistConfig = {
  key: "MyPet",
  storage,
};
const persistedReducer = persistReducer(persistConfig, reducers);

export default () => {
  const store = createStore(persistedReducer, undefined, undefined);
  const persistor = persistStore(store);
  return { store, persistor };
};
