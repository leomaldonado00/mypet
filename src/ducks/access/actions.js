import * as types from './types';

const login = (idUser, roleUser) => ({
    type: types.LOGIN_START,
    payload: { idUser,roleUser },
  });
  const createPatient = (source) => ({
    type: types.CREATE_PATIENT_START,
    payload: { source },
  });
  const logout = () => ({
    type: types.LOGOUT_START,
    payload: {},
  });

  export { login, createPatient, logout };

  