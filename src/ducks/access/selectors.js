import { get } from 'lodash';

export const source = state => get(state, 'accessStore');
export const roleUser = state => get(state, 'accessStore.roleUser');
export const idUser = state => get(state, 'accessStore.idUser');
