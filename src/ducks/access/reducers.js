import * as types from './types';
import json from '../../json/api.json'; // initial

const InitialState = json;
export default function accessStore(state = InitialState, action) {
  switch (action.type) {
    case types.LOGIN_START:
      return {
        ...state,
        idUser: action.payload.idUser,
        roleUser: action.payload.roleUser,
      };
      case types.CREATE_PATIENT_START:
        return {
          ...action.payload.source,
        };
      case types.LOGOUT_START:
        return {
          ...state,
          idUser: null,
          roleUser: null,
        };
    
    default:
      return state;
  }
}
