import React from 'react';
import { useSelector } from 'react-redux';
import { withRouter } from 'react-router';
import { source, idUser } from "../ducks/access";
// import { Link } from 'react-router-dom';
import styled from 'styled-components';

const Notifications = props => {
    const sourceTemp = useSelector((state) => source(state));
    const idUserTemp = useSelector((state) => idUser(state));
  const notifsArr = sourceTemp && sourceTemp.users[idUserTemp].notifications ?sourceTemp.users[idUserTemp].notifications : null; // arry notifications
  return (
      <Menu>
        <MenuItems>
        {notifsArr.length!==0 ? (
                    notifsArr.map((element, i) => (
                      <ItemMenu key={i}>
                        {`Eventualidad de ${element.event}: ${element.data}`}
                      </ItemMenu>
                    ))
                  ) : (
                    <ItemMenu value="no">No hay notificaciones</ItemMenu>
                  )}
        </MenuItems>
        
      </Menu>
  );
};
const Menu = styled.div`
overflow: auto;
cursor: auto;
  position: absolute;
  background: #ffffff;
  top: 55px;
  right: 30px;
  width: 300px;
  height: 350px;
  border-radius: 10px;
  -webkit-box-shadow: 0px 0px 27px -14px rgba(0, 0, 0, 0.75);
  -moz-box-shadow: 0px 0px 27px -14px rgba(0, 0, 0, 0.75);
  box-shadow: 0px 0px 27px -14px rgba(0, 0, 0, 0.75);

  @media (max-width: 720px) {
    right: 10px;
  }
 /* @media (min-width: 992px) {
    display: none;
  } */
`;
const MenuItems = styled.div`
  display: grid;
  width: 100%;
  height: 35px;
`;
const ItemMenu = styled.p`
  font-family: Montserrat;
  font-size: 14px;
  font-weight: 400;
  font-stretch: normal;
  font-style: normal;
  line-height: 2.86;
  letter-spacing: normal;
  text-align: left;
  color: #4a5763;
  margin: 0;
  text-align: center;

  padding: 3px 8px;

  border-bottom: 1px black solid;
`;
export default withRouter(Notifications);
