import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import styled from "styled-components";
import {useHistory} from "react-router-dom"
import { RiLogoutCircleFill } from "react-icons/ri";
import { FaDog } from "react-icons/fa";
import { roleUser, source, idUser, createPatient, logout } from "../ducks/access";
import Notifications from "./Notifications";

// import Text from './Text';

const Header = (props) => {
  const roleUserTmp = useSelector((state) => roleUser(state));
  const sourceTemp = useSelector((state) => source(state));
  const idUserTemp = useSelector((state) => idUser(state));

  const dispatch = useDispatch();
  const history = useHistory();

  const notifsArr =
    sourceTemp && sourceTemp.users[idUserTemp]
      ? sourceTemp.users[idUserTemp].notifications
      : null; // arry notifications
  const notifsLenght = notifsArr ? notifsArr.length : null; // length notifs

  const [botton, setBotton] = useState(false); // toggle noti

  function setBotton0() {
    setBotton(!botton);

    // setMenuLang(false);
    // cada vez que presionamos toggle gestiona la seen, actualizarla al  to notifi
    let sourceGral = JSON.stringify(sourceTemp);
    let sourceGralParse = JSON.parse(sourceGral);
    sourceGralParse.users[idUserTemp].seen = notifsLenght; // actualiza
    dispatch(createPatient(sourceGralParse));
  }

  function logout0() {
    dispatch(logout());
    history.push('/');
}

  return (
    <div>
      <HeaderDivMov bg={props.bg}>
        <Toggle onClick={() => logout0()}>
          <RiLogoutCircleFill size={25} color="white" />
          <h5>Salir</h5>
        </Toggle>

        <WrapArrowLogo>
          {/* props.arrow ? <ArrowBack history={props.history} /> : null */}
          <h2>MyPet</h2>
        </WrapArrowLogo>

        {roleUserTmp === "owner" ? (
          <Notif onClick={() => setBotton0()}>
            <FaDog size={25} color="white" />

            {notifsLenght &&
            sourceTemp &&
            notifsLenght !== sourceTemp.users[idUserTemp].seen ? (
              <h4>{notifsLenght - sourceTemp.users[idUserTemp].seen}</h4>
            ) : null}

            {botton ? <Notifications history={props.history} /> : null}
          </Notif>
        ) : (
          <Emp></Emp>
        )}
      </HeaderDivMov>
    </div>
  );
};

const WrapArrowLogo = styled.div`
  display: flex;
  margin: auto; /* center slf */
`;
const HeaderDivMov = styled.div`
  display: flex;
  position: fixed;
  top: 0px;
  z-index: 999999;
  width: 100%;
  background: ${(props) => props.bg || "silver"};
  height: 64px;
  align-items: center;
`;

const Toggle = styled.div`
  cursor: pointer;
  // position:absolute;  /* left slf */
  width: auto;
  height: 40px;
  display: flex;
  align-items: center;
  margin-left: 80px;
  @media (max-width: 620px) {
    margin-left: 10px;
  }
`;
const Notif = styled.div`
  cursor: pointer;
  // position:absolute;  /* left slf */
  width: auto;
  height: 40px;
  display: flex;
  align-items: center;
  margin-right: 80px;
  @media (max-width: 620px) {
    margin-right: 10px;
  }
`;
const Emp = styled.div`
  width: auto;
  height: 40px;
  display: flex;
  align-items: center;
  margin-right: 80px;
  @media (max-width: 620px) {
    margin-right: 10px;
  }
`;

export default Header;
