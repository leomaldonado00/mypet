import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import styled from "styled-components";
import {useHistory} from "react-router-dom"
import { login, source } from "../../ducks/access";
import Button from "../../components/Button";

const Login = () => {
  // const usersTemp = useSelector(state => console.log(state));
  const sourceTemp = useSelector(state => source(state));

  //Si quieres tener la ruta usa import {useHistory} from "react-router-dom" ya no tienes q pasar
  //history por props.
  const dispatch = useDispatch();
  const history = useHistory();

   const [formLogin, setFormLogin] = useState({
     user: ``,
     password: "",
     Euser: "",
     Epassword: "",
     Elogin: "",
   });

   const escrib = (e) => {
     if (e.target.name === "password") {
       if (e.target.value.length <= 20) {
         setFormLogin({
           ...formLogin,
           [e.target.name]: e.target.value,
           // [`E${e.target.name}`]: "",
           Elogin: "",
         });
          // setFlag(true);
       }
     } else if (e.target.value.length <= 50) {
       setFormLogin({
         ...formLogin,
         [e.target.name]: e.target.value,
         // [`E${e.target.name}`]: "",
         Elogin: "",
       });
        // setFlag(true);
     }
   };

   const validate0 = dataLogin => {
     const { user, password } = dataLogin;
     // const dataLoginF = { user, password };
      // dispatch(login(dataLoginF));
        // setFlag(false);

    // comprobar si esta entre los users:
    let usersArr = sourceTemp.users;
    for (let i = 0; i < usersArr.length; i++) {

      let userF = false;
      let passwordF = false;

        if (usersArr[i].user===user) {  // user
            userF=true;
        }else{
          userF=false;
        }  

        if (usersArr[i].password===password) {  // password
          passwordF=true;
        }else{
        passwordF=false;
        }  
        if(passwordF&&userF){ // both?
          // yes login // IDEAAAAAAAAAAAAAAAAAAAAA preguntar role y id, segun el role llevar a template, segun id buscar informacion de cliente...
          const idUser = usersArr[i].id;
          const roleUser = usersArr[i].role;
          dispatch(login(idUser,roleUser));
          if (roleUser==="vet") {
            history.push('/patient-list');
          }
          if(roleUser==="owner"){
            //tiene dog?
            const dogObj = usersArr[i].dog;
            if (JSON.stringify(dogObj)==='{}') {
              history.push('/create-patient'); // no have
            }else{
              history.push('/dashboard-patient'); // yes have
            }
          }
          // props.history.push(`/edit-${props.route}/${props.id}`);


          setFormLogin({
            ...formLogin,
            Elogin: ""
          });
          return
        }

    };
 // if here : nobody Not login
setFormLogin({
  ...formLogin,
  Elogin: "Verifique las credenciales"
});

  };

  return (
    <WrapAccess>
      <div>LOGIN MyPet</div>

      <Input
        name="user"
        type="text"
        placeholder=" Usuario"
        onChange={(e) => escrib(e)}
        value={formLogin.user}
      />

      <Input
        name="password"
        type="password"
        placeholder=" Contraseña"
       onChange={(e) => escrib(e)}
        value={formLogin.password}
      />

      <Button
        text="INICIAR SESIÓN"
        width="350px"
        marginTop="50px"
        heightButton="50px"
        onClick={() => {
           validate0(formLogin);
         }}
      />
      { formLogin.Elogin ? <h2>{formLogin.Elogin}</h2> : null}

    </WrapAccess>
  );
};

const Input = styled.input`
  width: 350px;
  height: 30px;
  border-radius: 5px;
  background-color: #f6f6f6;
  border: 1px solid #c8c8d0;
  margin: 0;
  padding-left: 5px;
  outline: none;
  color: gray;
  ::placeholder {
    color: #43425d;
  }
  @media (max-width: 480px) {
    width: calc(100% - 7px);
  }
`;
const WrapAccess = styled.div`
  width: 100%;
  min-height: 100vh;
  background: #f6f6f6;

  align-content: center;
  display: grid;
  justify-content: center;
`;

export default Login;
