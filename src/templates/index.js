import React from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import Login from "./access/Login";
import CreatePatient from "./owner/CreatePatient";
import DashboardPatient from "./owner/DashboardPatient";
import PatientDetail from "./vet/PatientDetail";
import PatientList from "./vet/PatientList";

const Routes = () => (
  <Router>
    <Route exact path="/" component={Login} />
    <Route exact path="/create-patient" component={CreatePatient} />
    <Route exact path="/dashboard-patient" component={DashboardPatient} />
    <Route exact path="/patient-list" component={PatientList} />
    <Route exact path="/patient-detail/:id" component={PatientDetail} />
  </Router>
);

export default Routes;
