import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import styled from "styled-components";
import { GrAddCircle } from "react-icons/gr";
import { useHistory } from "react-router-dom";
import { source, idUser, createPatient } from "../../ducks/access";
import Button from "../../components/Button";
import LayoutMyPet from "../../components/LayoutMyPet";

const DashboardPatient = () => {
  // const usersTemp = useSelector(state => console.log(state));
  const sourceTemp = useSelector((state) => source(state));
  const idUserTemp = useSelector((state) => idUser(state));

  const dispatch = useDispatch();
  const history = useHistory();

  const [formLogin, setFormLogin] = useState({
    name: ``,
    age: "",
    vaccinations: "",
    diseases: "",
    medicaments: "",
  });
  const [vaccinationsArr, setVaccinationsArr] = useState([]);
  const [medicamentsArr, setMedicamentsArr] = useState([]);
  const [diseasesArr, setDiseasesArr] = useState([]);
  const escrib = (e) => {
    const regx = / +/g;
    if (!/^\s/.test(e.target.value)) {
      if (
        e.target.name === "vaccinations" ||
        e.target.name === "diseases" ||
        e.target.name === "medicaments"
      ) {
        if (e.target.value !== "no") {
          setFormLogin({
            ...formLogin,
            [e.target.name]: e.target.value.replace(regx, " "),
            [`E${e.target.name}`]: "",
            Egral: "",
          });
        }
      } else if (e.target.value.length <= 50) {
        setFormLogin({
          ...formLogin,
          [e.target.name]: e.target.value.replace(regx, " "),
          [`E${e.target.name}`]: "",
        });
        // setFlag(true);
      }

      /* if (formLogin.email !== 0) {
          setFormLogin({ ...formLogin, Eemail: "" });
        } */
    }
  };
  const addVaccine = (vaccine) => {
    if (vaccine !== "") {
      setVaccinationsArr([...vaccinationsArr, vaccine.trim()]);
      setFormLogin({
        ...formLogin,
        vaccinations: "",
      });
      // setFlag(true);
    }
  };
  const addDisease = (disease) => {
    if (disease !== "") {
      setDiseasesArr([...diseasesArr, disease.trim()]);
      setFormLogin({
        ...formLogin,
        diseases: "",
      });
      // setFlag(true);
    }
  };
  const addMedicament = (medicament) => {
    if (medicament !== "") {
      setMedicamentsArr([...medicamentsArr, medicament.trim()]);
      setFormLogin({
        ...formLogin,
        medicaments: "",
      });
      // setFlag(true);
    }
  };

  const validate0 = (dataLogin, diseases, medicaments, vaccinations) => {
    let sourceGral = JSON.stringify(sourceTemp);
    let sourceGralParse = JSON.parse(sourceGral);
    const { name, age } = dataLogin; // obtener vars
    // preguntar en que etapa estan stage
    let stage;
    if (age <= 1) {
      stage = "Cachorro";
    } else if (age > 1 && age <= 3) {
      stage = "Adolescente";
    } else if (age > 3 && age <= 9) {
      stage = "Adulto";
    } else if (age > 9) {
      stage = "Adulto mayor";
    }
    // organizar datos
    const dataloginF = {
      name,
      age: parseInt(age),
      diseases,
      medicaments,
      vaccinations,
      stage,
    };
    // Reemplazar/agregar obj a dog
    sourceGralParse.users[idUserTemp].dog = dataloginF;
    // mandar a crear el perro actualizandocon la action
    if (name && age && diseases && medicaments && vaccinations && stage) {
      dispatch(createPatient(sourceGralParse));
      history.push("/dashboard-patient");
    }
  };

  return (
    <LayoutMyPet background="#f6f6f6" item="patient" bg="#00beb6">
      <WrapList display="grid" columns="auto">
        <h4>CREAR PACIENTE</h4>

        <h5>Nombre</h5>
        <Input
          name="name"
          type="text"
          placeholder=" Nombre"
          onChange={(e) => escrib(e)}
          value={formLogin.name}
          margin="0px 0px 7px 0px"
        />

        <h5>Edad</h5>
        <Input
          name="age"
          type="text"
          placeholder=" Edad"
          onChange={(e) => escrib(e)}
          value={formLogin.age}
          margin="0px 0px 7px 0px"
        />

        <h5>Vacunas</h5>
        <InpBut>
          <Select
            margin="0px 0px 7px 0px"
            name="vaccinations"
            onChange={(e) => escrib(e)}
            // error={formLogin.Eposition}
            value={formLogin.vaccinations}
          >
            <Option value="">Vacuna</Option>

            {sourceTemp && sourceTemp.vaccinations ? (
              sourceTemp.vaccinations.map((element, i) => (
                <Option key={i} value={element}>
                  {element}
                </Option>
              ))
            ) : (
              <Option value="no">Results were not loaded</Option>
            )}
          </Select>

          <AddIcon
            onClick={() => {
              addVaccine(formLogin.vaccinations);
            }}
          >
            <GrAddCircle size={25} color="#00beb6" />
          </AddIcon>
        </InpBut>

        {vaccinationsArr ? (
          <ol>
            {vaccinationsArr.map((element, i) => (
              <li key={i}>{element}</li>
            ))}
          </ol>
        ) : null}

        <h5>Enfermedades</h5>
        <InpBut>
          <Select
            margin="0px 0px 7px 0px"
            name="diseases"
            onChange={(e) => escrib(e)}
            // error={formLogin.Eposition}
            value={formLogin.diseases}
          >
            <Option value="">Enfermedad</Option>

            {sourceTemp && sourceTemp.diseases ? (
              sourceTemp.diseases.map((element, i) => (
                <Option key={i} value={element}>
                  {element}
                </Option>
              ))
            ) : (
              <Option value="no">Results were not loaded</Option>
            )}
          </Select>

          <AddIcon
            onClick={() => {
              addDisease(formLogin.diseases);
            }}
          >
            <GrAddCircle size={25} color="#00beb6" />
          </AddIcon>
        </InpBut>

        {diseasesArr ? (
          <ol>
            {diseasesArr.map((element, i) => (
              <li key={i}>{element}</li>
            ))}
          </ol>
        ) : null}

        <h5>Medicamentos</h5>
        <InpBut>
          <Select
            margin="0px 0px 7px 0px"
            name="medicaments"
            onChange={(e) => escrib(e)}
            // error={formLogin.Eposition}
            value={formLogin.medicaments}
          >
            <Option value="">Medicamento</Option>

            {sourceTemp && sourceTemp.medicaments ? (
              sourceTemp.medicaments.map((element, i) => (
                <Option key={i} value={element}>
                  {element}
                </Option>
              ))
            ) : (
              <Option value="no">Results were not loaded</Option>
            )}
          </Select>

          <AddIcon
            onClick={() => {
              addMedicament(formLogin.medicaments);
            }}
          >
            <GrAddCircle size={25} color="#00beb6" />
          </AddIcon>
        </InpBut>

        {medicamentsArr ? (
          <ol>
            {medicamentsArr.map((element, i) => (
              <li key={i}>{element}</li>
            ))}
          </ol>
        ) : null}

        <Button
          text="CREAR"
          width="200px"
          marginTop="20px"
          heightButton="35px"
          onClick={() => {
            validate0(formLogin, diseasesArr, medicamentsArr, vaccinationsArr);
          }}
        />
        {/* formLogin.Elogin ? <h2>{formLogin.Elogin}</h2> : null */}
      </WrapList>
    </LayoutMyPet>
  );
};

const Input = styled.input`
  width: 350px;
  height: 30px;
  border-radius: 5px;
  background-color: #f6f6f6;
  border: 1px solid #c8c8d0;
  margin: ${(props) => props.margin};
  padding-left: 5px;
  outline: none;
  color: gray;
  ::placeholder {
    color: #43425d;
  }
  @media (max-width: 480px) {
    width: calc(100% - 7px);
  }
`;
const WrapList = styled.div`
  width: 100%;
  display: ${(props) => props.display};
  justify-content: ${(props) => props.justifyContent};
  align-items: ${(props) => props.alingItems};
  grid-template-columns: ${(props) => props.columns};
  justify-items: center;
`;
const Select = styled.select`
  width: 357px;
  height: 36px;
  border-radius: 5px;
  background-color: #f6f6f6;
  border: 1px solid #c8c8d0;
  margin: ${(props) => props.margin};
  padding-left: 4px;
  outline: none;
  color: gray;
  @media (max-width: 480px) {
    width: calc(100% - 0px);
  }
`;
const Option = styled.option`
  color: gray;
`;
const InpBut = styled.div`
  display: flex;
  align-items: center;
  position: relative;
  justify-content: flex-end;
`;
const AddIcon = styled.div`
  position: absolute;
  display: flex;
  right: -32px;
  // height:36px;
  // width:36px;
  cursor: pointer;
`;
export default DashboardPatient;
