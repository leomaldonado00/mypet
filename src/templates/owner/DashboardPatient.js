import React from "react";
import { useSelector } from "react-redux";
import styled from "styled-components";
import { source, idUser } from "../../ducks/access";
import LayoutMyPet from "../../components/LayoutMyPet";

const DashboardPatient = () => {
  // const usersTemp = useSelector(state => console.log(state));
  const sourceTemp = useSelector((state) => source(state));
  const idUserTemp = useSelector((state) => idUser(state));
  const dogInfo = sourceTemp&&idUserTemp&&sourceTemp.users[idUserTemp].dog ? sourceTemp.users[idUserTemp].dog : null; // objdog

  return (
    <LayoutMyPet background="#f6f6f6" item="patient" bg="#00beb6">
      {dogInfo ?
          <WrapList display="grid" columns="auto">
        <h4>ESTADO DEL PERRO</h4>

        <h4>Nombre</h4>
        <div>{dogInfo.name}</div>

        <h4>Edad</h4>
        <div>{dogInfo.age}</div>

        <h4>Etapa</h4>
        <div>{dogInfo.stage}</div>

        <h4>Vacunas</h4>
        {dogInfo.vaccinations ? (
          <ol>
            {dogInfo.vaccinations.map((element, i) => (
              <li key={i}>{element}</li>
            ))}
          </ol>
        ) : null}

        
        <h4>Enfermedades</h4>
        {dogInfo.diseases ? (
          <ol>
            {dogInfo.diseases.map((element, i) => (
              <li key={i}>{element}</li>
            ))}
          </ol>
        ) : null}

        <h4>Medicamentos</h4>
        {dogInfo.medicaments ? (
          <ol>
            {dogInfo.medicaments.map((element, i) => (
              <li key={i}>{element}</li>
            ))}
          </ol>
        ) : null}

      </WrapList>
      : 
      <div>No hay informaion</div>
      }
    </LayoutMyPet>
  );
};

const WrapList = styled.div`
  width: 100%;
  display: ${(props) => props.display};
  justify-content: ${(props) => props.justifyContent};
  align-items: ${(props) => props.alingItems};
  grid-template-columns: ${(props) => props.columns};
  justify-items: center;
`;
export default DashboardPatient;
