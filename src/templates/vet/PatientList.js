import React from "react";
import { useSelector } from "react-redux";
import styled from "styled-components";
import { useHistory } from "react-router-dom";
import { source, idUser } from "../../ducks/access";
import LayoutMyPet from "../../components/LayoutMyPet";

const PatientList = () => {
  // const usersTemp = useSelector(state => console.log(state));
  const sourceTemp = useSelector((state) => source(state));
  const idUserTemp = useSelector((state) => idUser(state));
  const usersTemp = sourceTemp&&idUserTemp===0&&sourceTemp.users ? sourceTemp.users : null; // objdog

  const history = useHistory();

  return (
    <LayoutMyPet background="#f6f6f6" item="patient" bg="#00beb6">
      {usersTemp ?
          <WrapList display="grid" columns="auto">
        <h4>LISTA DE PACIENTES</h4>

        {usersTemp ? (
          <ol>
            {usersTemp.map((element, i) => (
              i!==0 && (
              <Li key={i} onClick={() => history.push(`/patient-detail/${element.id}`) }>
                <h4>Paciente</h4>
                <div>{element.dog&&element.dog.name?element.dog.name:"No registrado"}</div>
                <h4>Cliente</h4>
                <div>{element.name}</div>
                <hr/>
              </Li>
              )
            ))}
          </ol>
        ) : null}

      </WrapList>
      : 
      <div>No hay informaion</div>
      }
    </LayoutMyPet>
  );
};

const WrapList = styled.div`
  width: 100%;
  display: ${(props) => props.display};
  justify-content: ${(props) => props.justifyContent};
  align-items: ${(props) => props.alingItems};
  grid-template-columns: ${(props) => props.columns};
  justify-items: center;
`;
const Li = styled.li`
  cursor:pointer;
  :hover {
    background-color: ${props => props.bgh || 'silver'};
  }

`;

export default PatientList;
