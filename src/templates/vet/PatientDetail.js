import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import styled from "styled-components";
import { source, idUser, createPatient } from "../../ducks/access";
import LayoutMyPet from "../../components/LayoutMyPet";
import Button from "../../components/Button";

const PatientDetail = ({ match }) => {
  // const usersTemp = useSelector(state => console.log(state));
  const sourceTemp = useSelector((state) => source(state));
  const idUserTemp = useSelector((state) => idUser(state));

  const dispatch = useDispatch();


  const [formLogin, setFormLogin] = useState({
    event: "",
    vaccinations: "",
    diseases: "",
    medicaments: "",
  });

  const { id } = match && match.params ? match.params : null; // id de patient
  const userDetail =
    sourceTemp && sourceTemp.users[id] ? sourceTemp.users[id] : null; // todo del user
  const dogInfo =
    userDetail && JSON.stringify(userDetail.dog) !== "{}"
      ? userDetail.dog
      : null;

  const escrib = (e) => {
    const regx = / +/g;
    if (!/^\s/.test(e.target.value)) {
      if (
        e.target.name === "event" ||
        e.target.name === "vaccinations" ||
        e.target.name === "diseases" ||
        e.target.name === "medicaments"
      ) {
        if (e.target.value !== "no") {
          setFormLogin({
            ...formLogin,
            [e.target.name]: e.target.value.replace(regx, " "),
            [`E${e.target.name}`]: "",
            Egral: "",
          });
        }
      }

      /* if (formLogin.email !== 0) {
          setFormLogin({ ...formLogin, Eemail: "" });
        } */
    }
  };

  const validate0 = (dataLogin, event) => {

    let sourceGral = JSON.stringify(sourceTemp);
    let sourceGralParse = JSON.parse(sourceGral);

    // preguntar que eventualidad es para saber a cual agregar
    if (event === "Enfermedad") {
      //agregar la disease al arr del dog
      sourceGralParse.users[id].dog.diseases.push(dataLogin);
      // agregar notificacion
      sourceGralParse.users[id].notifications.unshift({
        event: event,
        data: dataLogin,
      });
      dispatch(createPatient(sourceGralParse));
    } else if (event === "Medicamento") {
      //agregar la disease al arr del dog
      sourceGralParse.users[id].dog.medicaments.push(dataLogin);
      // agregar notificacion
      sourceGralParse.users[id].notifications.unshift({
        event: event,
        data: dataLogin,
      });
      dispatch(createPatient(sourceGralParse));
    } else if (event === "Vacuna") {
      //agregar la disease al arr del dog
      sourceGralParse.users[id].dog.vaccinations.push(dataLogin);
      // agregar notificacion
      sourceGralParse.users[id].notifications.unshift({
        event: event,
        data: dataLogin,
      });
      dispatch(createPatient(sourceGralParse));
    }
  };

  return (
    <LayoutMyPet background="#f6f6f6" item="patient" bg="#00beb6">
      {userDetail&&idUserTemp===0 ? (
        <WrapList display="grid" columns="auto">
          <h4>DETALLE DEL PACIENTE</h4>

          <h4>Cliente</h4>
          <div>{userDetail.name}</div>

          {dogInfo ? (
            <div>
              <h4>Paciente</h4>
              <div>{dogInfo.name}</div>

              <h4>Edad</h4>
              <div>{dogInfo.age}</div>

              <h4>Etapa</h4>
              <div>{dogInfo.stage}</div>

              <h4>Vacunas</h4>
              {dogInfo.vaccinations ? (
                <ol>
                  {dogInfo.vaccinations.map((element, i) => (
                    <li key={i}>{element}</li>
                  ))}
                </ol>
              ) : null}

              <h4>Enfermedades</h4>
              {dogInfo.diseases ? (
                <ol>
                  {dogInfo.diseases.map((element, i) => (
                    <li key={i}>{element}</li>
                  ))}
                </ol>
              ) : null}

              <h4>Medicamentos</h4>
              {dogInfo.medicaments ? (
                <ol>
                  {dogInfo.medicaments.map((element, i) => (
                    <li key={i}>{element}</li>
                  ))}
                </ol>
              ) : null}
              <hr />
            </div>
          ) : (
            <div>No hay paciente registrado</div>
          )}

          {dogInfo ? (
            <>
              <h4>NOTIFICAR EVENTUALIDAD</h4>

              <>
                <h4>Vacuna:</h4>
                <Select
                  margin="0px 0px 7px 0px"
                  name="vaccinations"
                  onChange={(e) => escrib(e)}
                  // error={formLogin.Eposition}
                  value={formLogin.vaccinations}
                >
                  <Option value="">Vacuna</Option>

                  {sourceTemp && sourceTemp.vaccinations ? (
                    sourceTemp.vaccinations.map((element, i) => (
                      <Option key={i} value={element}>
                        {element}
                      </Option>
                    ))
                  ) : (
                    <Option value="no">Results were not loaded</Option>
                  )}
                </Select>

                <Button
                  text="NOTIFICAR"
                  width="200px"
                  marginTop="20px"
                  heightButton="35px"
                  onClick={() => {
                    validate0(formLogin.vaccinations, "Vacuna");
                  }}
                />
              </>

              <>
                <h4>Enfermedad:</h4>
                <Select
                  margin="0px 0px 7px 0px"
                  name="diseases"
                  onChange={(e) => escrib(e)}
                  // error={formLogin.Eposition}
                  value={formLogin.diseases}
                >
                  <Option value="">Enfermedad</Option>

                  {sourceTemp && sourceTemp.diseases ? (
                    sourceTemp.diseases.map((element, i) => (
                      <Option key={i} value={element}>
                        {element}
                      </Option>
                    ))
                  ) : (
                    <Option value="no">Results were not loaded</Option>
                  )}
                </Select>

                <Button
                  text="NOTIFICAR"
                  width="200px"
                  marginTop="20px"
                  heightButton="35px"
                  onClick={() => {
                    validate0(formLogin.diseases, "Enfermedad");
                  }}
                />
              </>

              <>
                <h4>Medicamento:</h4>
                <Select
                  margin="0px 0px 7px 0px"
                  name="medicaments"
                  onChange={(e) => escrib(e)}
                  // error={formLogin.Eposition}
                  value={formLogin.medicaments}
                >
                  <Option value="">Medicamento</Option>

                  {sourceTemp && sourceTemp.medicaments ? (
                    sourceTemp.medicaments.map((element, i) => (
                      <Option key={i} value={element}>
                        {element}
                      </Option>
                    ))
                  ) : (
                    <Option value="no">Results were not loaded</Option>
                  )}
                </Select>

                <Button
                  text="NOTIFICAR"
                  width="200px"
                  marginTop="20px"
                  heightButton="35px"
                  onClick={() => {
                    validate0(formLogin.medicaments, "Medicamento");
                  }}
                />
              </>
            </>
          ) : null}
        </WrapList>
      ) : (
        <div>No hay informaion</div>
      )}
    </LayoutMyPet>
  );
};

const WrapList = styled.div`
  width: 100%;
  display: ${(props) => props.display};
  justify-content: ${(props) => props.justifyContent};
  align-items: ${(props) => props.alingItems};
  grid-template-columns: ${(props) => props.columns};
  justify-items: center;
`;
const Select = styled.select`
  width: 357px;
  height: 36px;
  border-radius: 5px;
  background-color: #f6f6f6;
  border: 1px solid #c8c8d0;
  margin: ${(props) => props.margin};
  padding-left: 4px;
  outline: none;
  color: gray;
  @media (max-width: 480px) {
    width: calc(100% - 0px);
  }
`;
const Option = styled.option`
  color: gray;
`;

export default PatientDetail;
